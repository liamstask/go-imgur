package main

import (
	"io"
	"log"
	"mime"
	"net/http"
	"os"
	"strings"

	"bitbucket.org/liamstask/go-imgur/imgur"
)

const (
	clientID     = "YOUR_ID_HERE"
	clientSecret = "YOUR_SECRET_HERE"
)

var mimeExtensions = map[string]string{
	"image/gif":  ".gif",
	"image/png":  ".png",
	"image/jpeg": ".jpg",
}

func main() {

	// search for some images, and save them to disk

	client := imgur.NewClient(nil, clientID, clientSecret)

	log.Println("some recent keanu developments:")

	results, err := client.Gallery.Search("keanu", "time", 0)
	if err != nil {
		log.Fatal(err)
	}
	for i, r := range results {
		log.Println(i, ":", r.Title)
		if !r.IsAlbum {
			saveToDisk(r)
		}
	}

	rate, _, err := client.RateLimit()
	if err != nil {
		log.Println("Error fetching rate limit:", err)
	} else {
		log.Println("API Rate Limit:", rate)
	}
}

func saveToDisk(i imgur.GalleryImageAlbum) {

	t, _, err := mime.ParseMediaType(i.MimeType)
	if err != nil {
		log.Fatal(err)
	}

	ext, ok := mimeExtensions[t]
	if !ok {
		log.Println("unsupported mime type, skipping:", t)
		return
	}

	resp, err := http.Get(i.Link)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	fo, err := os.Create(filename(i.Title) + ext)
	if err != nil {
		log.Fatal(err)
	}
	defer fo.Close()

	if _, err = io.Copy(fo, resp.Body); err != nil {
		log.Fatal(err)
	}
}

func filename(t string) string {
	return strings.Map(func(r rune) rune {
		switch {
		// ~isalnum
		case 'a' <= r && r <= 'z' || 'A' <= r && r <= 'Z' || r == '_' || '0' <= r && r <= '9':
			return r
		case ' ' == r:
			return '-'
		}
		return -1
	}, t)
}
